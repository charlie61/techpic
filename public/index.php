<?php

define("DEBUG", 0);
define("ROOT", dirname(__DIR__));
define("PROD", ROOT . '/public');
define("COMPONENTS", ROOT . '/components');
define("CORE", ROOT . '/core');
define("LAYOUT", 'default');



$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";

$app_path = preg_replace("#[^/]+$#", '', $app_path);

$app_path = str_replace('/public/', '', $app_path);
define("PATH", $app_path);

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/config/routes.php';

new core\App();

