<?php
/**
 *
 */

namespace components\controllers;


use components\models\Profile;
use core\Controller;

class ProfileController extends Controller {


    public function indexAction(){
        $this->setTitle('Данные');

        $profile = new Profile();

//        $uri = $_SERVER['REQUEST_URI'];
//        $requestedUser = substr($uri, 20);
//        $requestedUser = str_replace('/', '', $requestedUser);
//        $_SESSION['requestedUser'] = $requestedUser;


        if(!empty($_SESSION['user'])){

            $profile->getOldData();
        }

        if($this->isAjax()){
            $profile->validateData();
            if(isset($_FILES['avatar'])){
                $profile->validateAvatar();
            }
            $profile->setNewData();
            $this->loadView('updated');
        }




    }

}