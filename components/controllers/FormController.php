<?php
/**
 *
 */

namespace components\controllers;


use components\models\Form;
use core\Controller;

class FormController extends Controller {


    public function indexAction(){
        $this->setTitle('Форма');
        $form = new Form();
        if($this->isAjax()){
            $form->validateData();
            if(isset($_FILES['avatar'])){
                $form->validateAvatar();
            }
            $form->saveData();
            $this->loadView('registry');
        }

    }

}