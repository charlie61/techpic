<?php if(isset($_SESSION['user'])): ?>
<div class="wrapper">
    <form action="" id="form" enctype="multipart/form-data" novalidate>
        <div class="form-row first-part">
            <div class="form-group col-md-6 form-row justify-content-center">

                <div class="rounded-circle  my-avatar">
                    <label class="label"  title="Change your avatar">
                        <img id="avatar" src="<?=PATH; ?>/img/<?php if($_SESSION['avatar']){echo $_SESSION['avatar'];}else{echo 'i.jpg';} ?>" alt="avatar">
                        <input type="file" class="sr-only" id="input" name="image" accept="image/*">
                    </label>
                </div>
                <!--        модальное окно        -->
                <div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col col-lg-8">
                                        <div class="img-container" id="canvas">
                                            <img id="image" src="" alt="Picture">
                                        </div>

                                    </div>
                                    <div class="col col-lg-4">
                                        <div class="justify-content-center text-center">
                                            <div class="preview">

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" id="crop" data-dismiss="modal">Обрезать</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--                модальное окно-->
            </div>
            <div class="form-group col-md-6">
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="ФИО">
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control" id="login" name="login" placeholder="Логин">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Е-почта">
                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-md-6">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                    </div>
                    <div class="form-group col-md-6">
                        <select class="custom-select" name="gender" id="gender">
                            <option selected value="1">Мужчина</option>
                            <option value="2">Женщина</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="second-part">
            <div class="form-row justify-content-center">
                <div class="form-group col-md-8">
                    <input type="text" class="form-control" id="city" name="city" placeholder="Город проживания">
                </div>
            </div>
            <div class="form-row justify-content-center">
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" id="phone1" name="phone1" placeholder="Первый телефон">
                </div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" id="phone2" name="phone2" placeholder="Второй телефон">
                </div>
            </div>
        </div>

        <div class="third-part justify-content-center text-center">
            <div class="form-row justify-content-center">
                <div class="form-group col-md-4">
                    <button type="reset" class="btn btn-primary">Очистить</button>
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
            </div>
        </div>

    </form>
</div>

<?php else:?>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 alert alert-danger justify-content-center text-center">
                <h4>Чтобы редактировать профиль, сначала нужно зарегестрироваться!</h4>
            </div>

        </div>
        <div class="row justify-content-center text-center">
            <div class="col-md-12">
                    <a class="btn btn-primary registry" href="<?=PATH; ?>/my_account" role="button">Внести данные</a>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
