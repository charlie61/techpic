<!doctype html>
<html lang="ru">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= $this->getTitle(); ?>
    <link rel="stylesheet" href="<?=PATH; ?>/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=PATH; ?>/css/style.css">
    <link rel="stylesheet" href="<?=PATH; ?>/lib/cropperjs-master/cropper.css">
</head>
<body>
<?=$content; ?>


<script src="<?=PATH; ?>/lib/jquery/jquery-3.3.1.min.js"></script>
<script src="<?=PATH; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=PATH; ?>/lib/cropperjs-master/cropper.js"></script>
<script src="<?=PATH; ?>/lib/jquery-validation/jquery.validate.js"></script>
<script src="<?=PATH; ?>/lib/jquery.maskedinput/jquery.maskedinput.js"></script>
<script src="<?=PATH; ?>/js/app.js"></script>
</body>
</html>