<?php
/**
 *
 */

namespace components\models;


use core\Db;
use core\Model;

class Form extends Model{

    public $errors = [];
    public $data = [];

    public function validateAvatar(){
        $this->data['avatar'] = '';
        $filePath  = $_FILES['avatar']['tmp_name'];
        $errorCode = $_FILES['avatar']['error'];

        if ($errorCode !== UPLOAD_ERR_OK || !is_uploaded_file($filePath)) {

            // Массив с названиями ошибок
            $errorMessages = [
                UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
                UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
                UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
            ];

            // Зададим неизвестную ошибку
            $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';

            // Если в массиве нет кода ошибки, скажем, что ошибка неизвестна
            $outputMessage = isset($errorMessages[$errorCode]) ? $errorMessages[$errorCode] : $unknownMessage;

            // Выведем название ошибки

            array_push($this->errors, $outputMessage);
        }

        // Создадим ресурс FileInfo
        $fi = finfo_open(FILEINFO_MIME_TYPE);

// Получим MIME-тип
        $mime = (string) finfo_file($fi, $filePath);

// Проверим ключевое слово image (image/jpeg, image/png и т. д.)
        if (strpos($mime, 'image') === false) {
            array_push($this->errors, 'Можно загружать только изображения.');
        }

        // Результат функции запишем в переменную
        $image = getimagesize($filePath);

// Зададим ограничения для картинок
        $limitBytes  = 1024 * 1024 * 5;
        $limitWidth  = 1280;
        $limitHeight = 768;

// Проверим нужные параметры
        if (filesize($filePath) > $limitBytes){
            array_push($this->errors,'Размер изображения не должен превышать 5 Мбайт.');
        }
        if ($image[1] > $limitHeight){
            array_push($this->errors, 'Высота изображения не должна превышать 768 точек.');
        }
        if ($image[0] > $limitWidth){
            array_push($this->errors,'Ширина изображения не должна превышать 1280 точек.');
        }


        // Сгенерируем новое имя файла на основе MD5-хеша
        $name = md5_file($filePath);

// Сгенерируем расширение файла на основе типа картинки
        $extension = image_type_to_extension($image[2]);

// Сократим .jpeg до .jpg
        $format = str_replace('jpeg', 'jpg', $extension);

// Переместим картинку с новым именем и расширением в папку /img
        if(empty($this->errors)){
            if (!move_uploaded_file($filePath, PROD . '/img/' . $name . $format)) {
                array_push($this->errors,'При записи изображения на диск произошла ошибка.');
            }
        }
        $avatar = $name.$format;
        $this->data['avatar'] = $avatar;
    }

    public function validateData(){
        if(!empty($_POST)){

//            проверка name

            if(isset($_POST['name'])){
                $name = strip_tags($_POST['name']);
                if(preg_match("/^([-А-Яа-я\s]{3,20})$/u", $name)){
                    $this->data['name'] = $name;
                }else{
                    array_push($this->errors, "Некорректное имя");
                }
            }else{
                array_push($this->errors, "Не хватает Вашего имени");
            }

//            проверка login

            if(isset($_POST['login'])){
                $login = strip_tags($_POST['login']);
                if(preg_match("/^([-0-9A-Za-z_]{3,20})$/", $login)){
                    $find = Db::$connections->query("SELECT `user_login`
                                                                    FROM `users`
                                                                    WHERE `user_login` = '{$login}';
                                                                    ");
                    $find_login = $find->fetch();
                    if(!$find_login){
                        $this->data['login'] = $login;
                    }else{
                        array_push($this->errors, "Такой логин уже существует!");
                    }

                }else{
                    array_push($this->errors, "Не корректный логин");
                }
            }else{
                array_push($this->errors, "Не хватает Вашего Логина");
            }

//          проверка e-mail

            if(isset($_POST['email'])){
                $email = strip_tags($_POST['email']);
                if(preg_match("/^([\d\w+_$@&]{1,})\@([a-z\d]{1,})\.([a-z\d]{2,8})$/", $email)){
                    $find = Db::$connections->query("SELECT `user_email`
                                                                    FROM `users`
                                                                    WHERE `user_email` = '{$email}';
                                                                    ");
                    $find_login = $find->fetch();
                    if(!$find_login){
                        $this->data['email'] = $email;
                    }else{
                        array_push($this->errors, "Такой е-майл уже зарегистрирован!");
                    }
                }else{
                    array_push($this->errors, "Не корректный е-мейл!");
                }

            }else{
                array_push($this->errors, "Не хватает Вашего е-мейл!");
            }

//            проверка password

            if(isset($_POST['password'])){
                $password = strip_tags($_POST['password']);
                if(preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[-_+#$])([-a-zA-Z0-9+_#$]{8,30})$/", $password)){
                    $this->data['password'] = md5($password);
                }else{
                    array_push($this->errors, "Не корректный пароль!");
                }

            }else{
                array_push($this->errors, "Не хватает Вашего пароля!");
            }

//            проверка gender

            if(isset($_POST['gender'])){
                $gender = strip_tags($_POST['gender']);
                if(preg_match("/^([1-2]{1})$/", $gender)){
                    $this->data['gender'] = $gender;
                }else{
                    array_push($this->errors, "Не корректный пол!");
                }

            }else{
                array_push($this->errors, "Укажите Ваш пол!");
            }

//            проверяем city

            if(isset($_POST['city'])){
                $city = strip_tags($_POST['city']);
                if(preg_match("/^([-А-Яа-я\s]{3,20})$/u", $city)){
                    $this->data['city'] = $city;
                }else{
                    array_push($this->errors, "Не корректно указан город!");
                }

            }else{
                array_push($this->errors, "Укажите Ваш город!");
            }

//            проверяем phone1


            if(isset($_POST['phone1'])){
                $phone1 = strip_tags($_POST['phone1']);
                if(preg_match("/^\(([0-9]{3})\)([0-9]{2})\-([0-9]{2})\-([0-9]{3})$/", $phone1)){
                    $this->data['phone1'] = $phone1;
                }else{
                    array_push($this->errors, "Не корректно указан номер телефона!");
                }

            }else{
                array_push($this->errors, "Укажите Ваш телефон!");
            }

//            проверяем phone2

            if(isset($_POST['phone2'])){
                $phone2 = strip_tags($_POST['phone2']);
                if($_POST['phone2']!=''){
                    if(preg_match("/^\(([0-9]{3})\)([0-9]{2})\-([0-9]{2})\-([0-9]{3})$/", $phone2)){
                        $this->data['phone2'] = $phone2;
                    }else{
                        array_push($this->errors, "Не корректно указан номер второго телефона!");
                    }
                }
            }

        }else{
            array_push($this->errors, "Вы не ввели данные!");
        }
    }

    public function saveData(){
        if(empty($this->errors)){
            Db::$connections->query("INSERT INTO `users`
                                                      SET `user_login` = '{$this->data['login']}',
                                                            `user_email` = '{$this->data['email']}',
                                                            `user_password` = '{$this->data['password']}',
                                                            `user_name` = '{$this->data['name']}',
                                                            `user_phone1` = '{$this->data['phone1']}',
                                                            `user_phone2` = '{$this->data['phone2']}',
                                                            `user_city` = '{$this->data['city']}',
                                                            `user_gender_id` = '{$this->data['gender']}',
                                                            `user_avatar` = '{$this->data['avatar']}';
                                                
                                                ");
            $_SESSION['user'] = $this->data['login'];
            $_SESSION['registry'] = 'success';

        }else{
            $_SESSION['errors'] = $this->errors;
        }
    }
}