<?php
/**
 *
 */

namespace core;


class Router{

    protected static $routes = [];
    protected static $route = [];

    public static function add($regexp, $route = []){

        self::$routes[$regexp] = $route;

    }

    public static function dispatch($url){
        if(self::matchRoute($url)){
           $controller = 'components\controllers\\' .self::$route['controller'].'Controller';
           if(class_exists($controller)){
                $controllerObj = new $controller(self::$route);
                $action = self::$route['action'] . 'Action';
                if(method_exists($controllerObj, $action)){
                    $controllerObj->$action();
                    $controllerObj->getView();
                }else{
                    throw new \Exception("Метод $controller::$action не найден", 404);
                }

           }else{
               throw new \Exception("Контроллер $controller не найден", 404);
           }
        }else{
            throw new \Exception("Страница не найдена", 404);
        }
    }

    public static function matchRoute($url){

        foreach (self::$routes as $pattern => $route){
            if(preg_match("#{$pattern}#", $url, $matches)){
                foreach ($matches as $k => $v){
                    if(is_string($k)){
                        $route[$k] = $v;
                    }
                }
                self::$route = $route;
                return true;
            }
        }
        return false;
    }
}