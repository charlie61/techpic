<?php
/**
 *
 */

namespace core;


class View{

    public $controller;
    public $route;
    public $model;
    public $view;
    public $title;
    public $layout;
    public $data = [];

    public function __construct($route, $layout = '', $view = '', $title){
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $view;
        $this->model = $route['controller'];
        $this->title = $title;
        if($layout === false){
            $this->layout = false;
        }else{
            $this->layout = $layout ?: LAYOUT;
        }
    }

    public function render($data){
        if(is_array($data)) extract($data);
        $viewFile = COMPONENTS . "/views/{$this->controller}/{$this->view}.php";
        if(is_file($viewFile)){
            ob_start();
            require_once $viewFile;
            $content = ob_get_clean();
        }else{
            throw new \Exception("Не найден вид {$viewFile}", 500);
        }

        if(false !== $this->layout){
            $layoutFile = COMPONENTS . "/views/layouts/{$this->layout}.php";
            if(is_file($layoutFile)){
                require_once $layoutFile;
            }else{
                throw new \Exception("Не найден шаблон", 500);
            }
        }
    }

    public function getTitle(){
        $output = '<title>'. $this->title .'</title>';
        return $output;
    }
}