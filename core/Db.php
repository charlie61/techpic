<?php
/**
 *
 */

namespace core;

class Db{

    public static $connections;

    use TSingletone;

    protected function __construct(){
        $paramsPath = ROOT . '/config/db_params.php';
        $params = include($paramsPath);
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset={$params['charset']}";
        $opt = array(\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC);
        try {
            $pdo = new \PDO($dsn, $params['user'], $params['password'], $opt);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch(\PDOException $e){
            throw new \Exception("Нет соединения с БД {$e->getMessage()}", 404);
        }
        self::$connections =  $pdo;
    }
}