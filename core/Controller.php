<?php
/**
 *
 */

namespace core;


abstract class Controller{

    public $controller;
    public $route;
    public $model;
    public $view;
    public $title = '';
    public $layout;
    public $data = [];

    public function __construct($route){
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $route['action'];
        $this->model = $route['controller'];
    }

    public function set($data){
        $this->data = $data;
    }

    public function setTitle($title = ''){
        $this->title = $title;
    }

    public function getView(){

        $viewObject = new View($this->route, $this->layout, $this->view, $this->title);
        $viewObject->render($this->data);
    }

    public function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    public function loadView($view){
        require COMPONENTS . "/views/ajaxViews/{$view}.php";
        die;
    }
}